# Suivi Projet

Sujet 3 : Poids du cartable

## Tableau de suivi

Priorité | Tâche | Date de création | Temps estimé | Temps réel | Attribué à | Effectué le |
-------- | ------ | ---------------- | ------------ | ---------- | ---------- | ----------- |
Haute | CU Consultation des fournitures | 18/09 | 30min | | Thibaut |
Haute | CU Consultation du poids moyen | 18/09 | 30min| 30min | Christopher | 23/09
Haute | Finir de remplir le tableau d'analyse | 18/09 | 1h | | Lucas & Paul |
Moyenne | Configuration du repo git | 18/09 | 30min | | Lucas |
Basse | Définition des règles de contribution  | 18/09 | 1h | | Tout le monde |
Moyenne | Corrections et mise en forme | 18/09 | 1h | | Tao |


## Suivi des séances

### Mardi 11 Septembre 2018

Présents:

- Tao Galasse
- Christopher Anciaux
- Paul Delafosse
- Thibaut Strecker
- Lucas Declercq

### Objectifs de la scéance

- Définition du sujet
- Identification du problème
- Établissement des premiers scénarios

### Déroulement

Les décisions ont été adoptées à l'unanimité, après débat.

### Mardi 18 septembre 2018

Présents :

- Thibaut
- Christopher
- Tao
- Lucas
- Paul


### Objectifs de la séance

Mettre les scénarios précédemment établis sous la forme de diagrammes de cas d'utilisation.
Identifier les acteurs, les actions effectuées par ces derniers et le sujet des actions.


### Déroulement

Les premiers diagrammes de cas d'utilisation que nous avons établis se sont avérés trop détaillés.
Nous avons donc décidé de les reprendre en les simplifiant, dans le but d'éviter de commettre des erreurs d'analyse.
Nous nous sommes répartis les diagrammes restant à la fin de la séance.
Il nous reste à mettre en place une organisation efficace du travail, en particulier les règles de valdiation des commits sur le repository.
Il faut également enrichir le tableau d'ananalyse et d'y rajouter les contraintes.



NB : une seule personne du groupe (Lucas) a accès au compte Lille 1. Nous allons travailler dans un premier temps sur framagit.
