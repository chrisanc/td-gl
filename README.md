# Cailloux Project (temporaire)

# Problème

Comment réduire le poids du cartable d'un élève ?

Un manque d'organisation et de communication entre le corps enseignant, les parents et les élèves entraine un poids trop important des sacs de ces derniers.

## Scénarios

### Connection à l'application

Tout élève / parent / professeur / personnel de l'administration peut se connecter à l'application

### Édition de la liste standard de fournitures

Tout membre du personnel administratif peut créer / modifier / supprimer les fournitures de la _liste standard_.

### Édition de la liste professeur

Tout professeur peut ajouter / modifier / supprimer des éléments de sa liste professeur.

#### Détails

Le professeur renseigne les caractéristiques de chaque élèments qu'il ajoute (nom, poids, etc...)

### Édition de la liste classe

Tout professeur peut définir la liste des fournitures annuelles de ses classes.

#### Details

Le professeur demande la liste de ses classes.

Il choisit l'une d'entre elles.

Il choisit une référence venant de la _liste standard_ ou de sa liste professeur et l'assigne à la classe.

### Édition de la liste élève

Tout élève ou parent peut créer / modifier / supprimer les fournitures de sa liste élève

#### Détails

Le parent / élève affiche la liste élève, prérenseignée avec les éléments des listes classes.

Le parent / élève ajoute des éléments personnels à la liste.

Le parent / élève renseigne précisément les caractéristiques de chaque élément dans sa liste personnelle (nom, poids, etc...)

### Consultation de l'emploi du temps et du poids du sac associé

Tout professeur / membre de l'administration peut consulter l'emploi du temps d'une journée et le poids du sac associé des élèves.
Un élève (ou ses parents) peut consulter son emploi du temps et le poids du sac associé.

### Consultation des fournitures nécessaires pour un jour donné

Tout élève (ou ses parents) peut consulter la liste des fournitures nécessaires pour une journée donnée et du poids associé.

### Alerte poids

Les professeurs concernés et l'administration sont notifiés lorsque le poids du sac pour une journée est trop important.

#### Détails

Lorsque le poids du sac d'une classe pour une journée donnée dépasse les limites établies, les professeurs concernés et l'administration recoivent une alerte.

Ils modifient les listes classes pour atteindre un poids en dessous de la limite.

### Consultation du poids moyen pour chaque classe

Les __utilisateurs__ peuvent consulter les poids moyens des sacs pour chaque classe.

## Glossaires

### Glossaire technique

- __Utilisateurs__ : comprends élèves / parents / professeurs / personnel administratif
- __Liste standard__ : liste de fournitures préétablie par l'administration comportant des fournitures génériques associées à leur poids.
- __Liste professeur__ : liste associée à un professeur comprenant des fournitures que le professeur à personnellement choisi et qui n'étaient pas présentes dans la liste standard.
- __Liste classe__ : liste définie par un professeur, associée à une classe et une matière
- __Liste élève__ : liste des effets personnels d'un élève

### Glossaire métier